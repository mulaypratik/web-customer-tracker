<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer List</title>
<!-- Reference our style sheet -->
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/image/favicon.png"
	type="image/gif" sizes="16x16">
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<h2>Customer Relationship Manager (CRM)</h2>
		</div>
	</div>

	<div id="container">
		<div id="content">

			<!-- Put new button: Add Customer -->

			<input type="button" value="Add Customer"
				onclick="window.location.href='showFormForAdd'; return false;"
				class="add-button" />

			<!-- Our HTML table here -->
			<table>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>

				<c:forEach var="customer" items="${customers}">

					<!-- Construct an 'update' link with customer id -->
					<c:url var="updateLink" value="/customer/showFormForUpdate">
						<c:param name="customerId" value="${customer.intId}"></c:param>
					</c:url>

					<c:url var="deleteLink" value="/customer/delete">
						<c:param name="customerId" value="${customer.intId}"></c:param>
					</c:url>

					<tr>
						<td>${customer.strFirstName}</td>
						<td>${customer.strLastName}</td>
						<td>${customer.strEmail}</td>
						<td><a href="${updateLink}">Update</a>|<a
							href="${deleteLink}"
							onclick="if (!(confirm('Are you sure you want o delete this customer?'))) return false">Delete</a></td>
					</tr>

				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>