<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Form</title>
<!-- Reference our style sheet -->
<link rel="icon"
	href="${pageContext.request.contextPath}/resources/image/favicon.png"
	type="image/gif" sizes="16x16">
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/add-customer-style.css" />
</head>
<body>
	<div id="wrapper">
		<div id="header">
			<h2>Customer Relationship Manager (CRM)</h2>
		</div>
	</div>

	<div id="container">
		<h3>Save Customer</h3>

		<form:form action="saveCustomer" modelAttribute="customer"
			method="POST">

			<!-- Need to associate this data with customer id -->
			<form:hidden path="intId" />

			<table>
				<tbody>

					<tr>
						<td><label>First Name:</label></td>
						<td><form:input path="strFirstName" /></td>
					</tr>

					<tr>
						<td><label>Last Name:</label></td>
						<td><form:input path="strLastName" /></td>
					</tr>

					<tr>
						<td><label>Email:</label></td>
						<td><form:input path="strEmail" /></td>
					</tr>

					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>

				</tbody>
			</table>

		</form:form>

		<p>
			<a href="${pageContext.request.contextPath}/customer/list">Back
				to list</a>
		</p>

	</div>
</body>
</html>