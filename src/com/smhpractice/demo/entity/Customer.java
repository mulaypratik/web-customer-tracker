package com.smhpractice.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int intId;

	@Column(name = "first_name")
	private String strFirstName;

	@Column(name = "last_name")
	private String strLastName;

	@Column(name = "email")
	private String strEmail;

	public Customer() {
	}

	public Customer(String strFirstName, String strLastName, String strEmail) {
		this.strFirstName = strFirstName;
		this.strLastName = strLastName;
		this.strEmail = strEmail;
	}

	public int getIntId() {
		return intId;
	}

	public void setIntId(int intId) {
		this.intId = intId;
	}

	public String getStrFirstName() {
		return strFirstName;
	}

	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}

	public String getStrLastName() {
		return strLastName;
	}

	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}

	public String getStrEmail() {
		return strEmail;
	}

	public void setStrEmail(String strEmail) {
		this.strEmail = strEmail;
	}

	@Override
	public String toString() {
		return "Customer [intId=" + intId + ", strFirstName=" + strFirstName + ", strLastName=" + strLastName
				+ ", strEmail=" + strEmail + "]";
	}
}
