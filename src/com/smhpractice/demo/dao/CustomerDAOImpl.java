package com.smhpractice.demo.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.smhpractice.demo.entity.Customer;

@Repository
public class CustomerDAOImpl implements ICustomerDAO {
	// Need to add session factory
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Customer> getCustomers() {
		// Get the current hibernate session
		Session session = sessionFactory.getCurrentSession();

		// Create a query
		Query<Customer> qCustomer = session.createQuery("from Customer order by strLastName", Customer.class);

		// Execute and get result list
		List<Customer> customers = qCustomer.getResultList();

		// This is also okay --
		// List<Customer> customers = session.createQuery("from Customer",
		// Customer.class).getResultList();
		// --

		// Return the results
		return customers;
	}

	@Override
	public void saveCustomer(Customer customer) {
		Session session = sessionFactory.getCurrentSession();

		session.saveOrUpdate(customer);
	}

	@Override
	public Customer getCustomer(int customerId) {
		// Get the current hibernate session
		Session session = sessionFactory.getCurrentSession();

		// Retrieve data from database using the primary key
		Customer customer = session.get(Customer.class, customerId);

		return customer;
	}

	@Override
	public void deleteCustomer(int customerId) {
		// Get the current hibernate session
		Session session = sessionFactory.getCurrentSession();

		// First method
//		System.out.println("By first method...");
//
//		// Get Customer object to be deleted
//		Customer customer = session.get(Customer.class, customerId);
//
//		// Delete the object
//		session.delete(customer);
		// ------------------------

		// Second method
		System.out.println("By Second method...");

		@SuppressWarnings("unchecked")
		Query<Customer> query = session.createQuery("delete from Customer where intId=:customerId");
		query.setParameter("customerId", customerId);
		query.executeUpdate();
		// ------------------------
	}
}
