package com.smhpractice.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.smhpractice.demo.entity.Customer;
import com.smhpractice.demo.service.ICustomerService;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	// Need to inject the Customer Service
	@Autowired
	private ICustomerService customerService;

	@GetMapping("/list")
	public String listCustomers(Model mdlCUstomer) {
		// Get customers from the Service
		List<Customer> customers = customerService.getCustomers();

		// Add the customers to the model
		mdlCUstomer.addAttribute("customers", customers);

		return "list-customers";
	}

	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model customerModel) {
		// Create a model attribute to bind the data
		Customer customer = new Customer();

		customerModel.addAttribute("customer", customer);

		return "customer-form";
	}

	@PostMapping("/saveCustomer")
	public String saveCustomer(@ModelAttribute("customer") Customer customer) {
		// Save the customer using service
		customerService.saveCustomer(customer);

		return "redirect:/customer/list";
	}

	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("customerId") int customerId, Model customerModel) {
		// Get customer from database
		Customer customer = customerService.getCustomer(customerId);

		// Set custmer as a model attribute to pre-populate the form
		customerModel.addAttribute("customer", customer);

		// Send over to our form
		return "customer-form";
	}

	@GetMapping("/delete")
	public String deleteCustomer(@RequestParam("customerId") int customerId) {
		// Delete the customer for given id
		customerService.deleteCustomer(customerId);

		return "redirect:/customer/list";
	}
}
